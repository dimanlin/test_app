'use strict'


postList.controller('TagListCtrl', function($scope, $http) {
  $scope.tags = []

  $http.get('/api/tags.json').success(function(data) {
    $scope.tags = data
  })

  $scope.searchByTag = function($event) {
    $scope.ctag = $event.currentTarget.getAttribute("data-tag")
    $scope.$parent.$broadcast('searchPostsByTags', $scope.ctag);
  }
})
