'use strict'
var postList = angular.module('postList', [])

postList.controller('PostListCtrl', function($scope, $http, $httpParamSerializer) {
  $scope.currentPage = 1
  $scope.perPage = 10
  $scope.pages = []
  $scope.tag = ''

  $scope.$on('searchPostsByTags', function(event, data) {
    $scope.currentPage = 1
    $scope.tag = data
    $scope.getPosts()
  })

  $scope.$watch('currentPage', function(newValue, oldValue) {
    if(newValue != oldValue) {
      $scope.getPosts()
    }
  });

  $scope.pageChange = function(page) {
    $scope.currentPage = page
  }

  $scope.getPosts = function() {
    $scope.url_params = $httpParamSerializer({page: $scope.currentPage, tag: $scope.tag})
    $http.get("api/posts.json?" + $scope.url_params).success(function(data) {
      $scope.posts = data.posts
      $scope.pages = []

      for (var i = 1; i <= data.total_pages; i++) {
        $scope.pages.push(i)
      }
    })
  }

  $scope.getPosts()
})
