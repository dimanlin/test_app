'use strict'

postList.config([
  "$httpProvider", function($httpProvider) {
    $httpProvider.defaults.headers.common['X-CSRF-Token'] = $('meta[name=csrf-token]').attr('content');
  }
]);

postList.controller('PostShowCtrl', function($scope, $http) {
  $scope.post = undefined
  $scope.comments = []
  $scope.comment_body = ''
  $scope.comment_data = undefined
  $scope.current_user = undefined

  if($scope.post != undefined) {
    $scope.post_url = "api/post/" + $scope.post.id
    $http.get($scope.post_url).success(function(data) {
      $scope.post = data
    })
  }

  $scope.$watch('post_id', function(newValue, oldValue) {
    $scope.post_url = "/api/posts/" + newValue + '.json'
  })

  $scope.$watch('post_url', function(newValue, oldValue) {
    $scope.getPost()
  })

  $scope.getPost = function() {
    $http.get($scope.post_url).success(function(data) {
      $scope.post = data
      $scope.comments = data.comments
    })
  }

  $scope.deleteComment = function(comment_id) {

    $scope.comment = _.findWhere($scope.comments, {id: comment_id})
    $http.delete($scope.comment.delete_url).success(function() {
      $scope.getPost()
    }).error(function() {
      alert('Вы не можете удалить коментрий который был создан более 15 минут назад.')
    })
  }

  $scope.hendlerCommentBody = function ($event) {
    $scope.comment_body = $event.currentTarget.value
  }

  $scope.hendlerCreateComment = function() {
    if($scope.comment_body != undefined && $scope.comment_body.length > 0) {
      $scope.comment_data = { comment: {post_id: $scope.post.id, body: $scope.comment_body} }
      $http.post($scope.post.create_comment_url, $scope.comment_data).success(function(data) {
        $scope.comment_body = ''
        $scope.getPost()
      })
    }
  }
})
