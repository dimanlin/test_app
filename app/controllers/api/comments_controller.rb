class Api::CommentsController < Api::ApplicationController
  before_action :authenticate_user!

  def create
    post = Post.find(params[:post_id])
    post.comments.create(comment_params.merge(user_id: current_user.id))
    redirect_to post_path(post)
  end

  def destroy
    comment = current_user.comments.find(params[:id])
    decorator_comment = CommentDecorator.new(comment)
    @result = decorator_comment.destroy
    head :locked if @result == false
  end

  private

  def comment_params
    params.require(:comment).permit(:body, :post_id)
  end
end
