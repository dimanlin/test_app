class Api::PostsController < Api::ApplicationController

  def index
    @posts = Post.active.includes(:user)
    @posts = if params[:tag].present?
              @posts.by_tag(params[:tag])
            else
              @posts
            end.paginate(per_page: 5, page: params[:page])  
  end

  def show
    @post = Post.active.find(params[:id])
  end

end
