class Api::TagsController < Api::ApplicationController
  def index
    tags = Post.tags
    render json: tags.to_json
  end
end
