class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  
  before_filter :init_tags

  private

  def init_tags
    @tags = Post.tags
  end
end
