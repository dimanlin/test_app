class Dashboard::PostsController < Dashboard::ApplicationController
  before_action :init_post_by_id, only: [:edit, :update, :destroy, :publish, :hide]

  def index
    @posts = current_user.posts
  end

  def edit
  end

  def update
    if @post.update(post_params)
      redirect_to dashboard_posts_path, nitice: '.controllers.dashboard.posts.update.success'
    else
      render action: 'edit'
    end
  end

  def destroy
    @post.destroy
    redirect_to dashboard_posts_path, notice: notice
  end

  def new
    @post = Post.new(tags: '')
  end

  def create
    @post = current_user.posts.new(post_params)
    if @post.save
      redirect_to dashboard_posts_path, notice: I18n.t('.controllers.posts.create.post_was_created')
    else
      render action: 'new'
    end
  end

  def publish
    @post.publish!
    redirect_to dashboard_posts_path
  end

  def hide
    @post.hide!
    redirect_to dashboard_posts_path
  end

  private

  def init_post_by_id
    @post = current_user.posts.find(params[:id])
  end

  def post_params
    params.require(:post).permit(:created_at, :body, :hide, :title, :string_tags)
  end
end
