class CommentDecorator
  def initialize(comment)
    @comment = comment
  end

  def destroy
    if @comment.created_at > @comment.class::DELETE_TIME.minutes.ago
      @comment.destroy
    else
      false
    end
  end
end
