class Comment < ApplicationRecord
  DELETE_TIME = 15 #min

  validates :user_id, :post_id, :body, presence: true
  belongs_to :user
  belongs_to :post

  default_scope { order(created_at: :desc) }
end
