module Paginate
  extend ActiveSupport::Concern
  def paginate(options = {})
    default_options = { per_page: 10, page: 1 }
    options = default_options.merge(options)

    options[:page] = 1 if options[:page].nil?
    options[:per_page] = 10 if options[:per_page].nil?

    off_set = (options[:page].to_i * options[:per_page].to_i) - options[:per_page].to_i
    rel = offset(off_set).limit(options[:per_page])
    rel.class.module_eval { attr_accessor :total_pages}

    rel.total_pages = count_pages(options[:per_page])
    rel
  end

  def count_pages(per_page)
    (self.count / per_page.to_f).ceil
  end
end
