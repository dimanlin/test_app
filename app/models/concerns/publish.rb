module Publish
  extend ActiveSupport::Concern
  def publish!
    update(hide: false)
  end

  def hide!
    update(hide: true)
  end
end
