module Tags
  extend ActiveSupport::Concern

  def string_tags=(val)
    self.tags = val.split(',')
  end

  def string_tags
    self.tags ? self.tags.join(',') : self.tags
  end
end
