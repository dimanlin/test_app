class Post < ApplicationRecord
  include Publish
  include Tags
  extend Paginate

  belongs_to :user
  has_many :comments

  validates :title, :user_id, :body, presence: true

  default_scope { order(created_at: :desc) }
  scope :active, -> { where(hide: false).includes(:comments) }
  scope :by_tag, lambda { |a| where("'#{a}' = ANY (tags)") }

  def self.tags
    Post.active.pluck(:tags).flatten.uniq
  end

end
