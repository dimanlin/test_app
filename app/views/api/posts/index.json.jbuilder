json.total_pages @posts.total_pages
json.posts do
  json.array! @posts do |post|
    json.partial! 'api/shared/post', post: post
  end
end
