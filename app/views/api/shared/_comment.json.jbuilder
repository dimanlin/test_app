json.id comment.id
json.user_id comment.user_id
json.post_id comment.post_id
json.body comment.body
json.created_at comment.created_at
json.updated_at comment.updated_at
json.delete_url api_post_comment_path(post_id: comment.post, id: comment.id, format: :json)
