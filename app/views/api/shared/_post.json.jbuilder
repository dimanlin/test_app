json.id post.id
json.user_id post.user_id
json.tags post.tags.join(', ')
json.title post.title
json.body post.body
json.created_at post.created_at
json.updated_at post.updated_at
json.hide post.hide
json.url post_path(post)
json.create_comment_url api_post_comments_path(post_id: post.id)

json.comments do
  json.array! post.comments do |comment|
    puts comment.inspect
    json.partial! 'api/shared/comment', comment: comment
  end
end
json.user do
  json.partial! 'api/shared/user', user: post.user
end
