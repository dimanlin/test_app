module ActionView
  module CompiledTemplates
    class CustomFormBuilder < ActionView::Helpers::FormBuilder
      def tags(method, tag_value, options = {})
        default_options = { class: 'tags_field'}
        options = default_options.merge(options)
        @template.content_tag(:div,
          @template.text_area(
            @object_name, method, objectify_options(options)
          )
        )
      end
      def text_field(method, tag_value, options = {})
        default_options = { class: 'form-control'}
        options = default_options.merge(options)
        @template.content_tag(:div,
          @template.content_tag(:label, I18n.t("activerecord.attributes.#{object.class.to_s.downcase}.#{method}")).concat(
            if object.errors.full_messages_for(method)
              @template.content_tag(:div, object.errors.full_messages_for(method).join(', '), class: 'error').concat(
                @template.text_field(@object_name, method, objectify_options(options))
              )
            else
              @template.text_field(@object_name, method, objectify_options(options))
            end
          ), class: 'form-group')
      end

      def text_area(method, tag_value, options = {})
        default_options = { class: 'form-control'}
        options = default_options.merge(options)
        @template.content_tag(:div,
          @template.content_tag(:label, I18n.t("activerecord.attributes.#{object.class.to_s.downcase}.#{method}")).concat(
            if object.errors.full_messages_for(method)
              @template.content_tag(:div, object.errors.full_messages_for(method).join(', '), class: 'error').concat(
                @template.text_area(@object_name, method, objectify_options(options))
              )
            else
              @template.text_area(@object_name, method, objectify_options(options))
            end
          ), class: 'form-group')
      end
    end
  end
end
