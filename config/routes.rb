Rails.application.routes.draw do
  devise_for :users
  root 'posts#index'

  namespace :api do
    resources :tags, only: :index
    resources :posts do
      resources :comments
    end
  end

  namespace :dashboard do
    resources :posts do
      member do
        post :publish
        post :hide
      end
      resources :comments
    end
  end

  resources :posts, only: [:index, :show]
end
