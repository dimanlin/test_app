class CreatePosts < ActiveRecord::Migration[5.0]
  def change
    create_table :posts do |t|
      t.column :user_id, :integer
      t.text :tags, array: true, default: []
      t.column :title, :string
      t.column :body, :text
      t.timestamps
    end
  end
end
