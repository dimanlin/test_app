class AddColumnHideToPosts < ActiveRecord::Migration[5.0]
  def change
    add_column :posts, :hide, :boolean, default: false
  end
end
