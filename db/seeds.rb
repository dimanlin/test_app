[User, Comment, Post].map(&:destroy_all)

tags = ['Audio', 'Video', 'Rss', 'Rails', 'Development', 'C++', 'ubuntu']

10.times do
  user = User.create(email: FFaker::Internet.email,
              password: 'root0000',
              password_confirmation: 'root0000')

  17.times do
    post = user.posts.create(title: FFaker::Lorem.words(rand(7) + 3).join(' '),
                              body: FFaker::Lorem.paragraph,
                              tags: tags.sample(1 + rand(2)),
                              hide: [true, false].sample)
    4.times do
      post.comments.create(user_id: User.all.sample.id,
                            body: FFaker::Lorem.paragraph)
    end
  end
end
